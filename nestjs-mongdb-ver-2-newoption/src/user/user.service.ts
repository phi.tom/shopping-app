import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
const jwt = require('jsonwebtoken');
const bcrypt = require("bcrypt");

import { User } from './user.model'

const fs = require('fs')

let privateKey
let publicKey

try {
    const data = fs.readFileSync('src/user/private.key', 'utf8')
    const datapublic = fs.readFileSync('src/user/public.key', 'utf8')
    privateKey = data
    publicKey = datapublic
    // console.log(data)
} catch (err) {
    console.error(err)
}

// const privateKey = process.env.KEY_PRIVATE;



// console.log(privateKey);


const iss = "https://securetoken.google.com/shopping-app-f29b6"
const sub = "thanhtung19041998@gmail.com"
const aud = "shopping-app-f29b6"
const exp = "1h"


var signOptions = {
    issuer: iss,
    audience: aud,
    expiresIn: exp,
    keyid: "650ffeefb5f79c3d10af106b9a5daca636cf9774",
    algorithm: "RS256",
};

var verifyOptions = {
    issuer: iss,
    audience: aud,
    maxAge: exp,
    algorithms: ["RS256"]
};





function getTimestamp(date) {
    var tp = Math.round(Date.parse(date) / 1000);
    // console.log(tp)
    return tp;
}





const generateAccessToken = (user: any) => {
    return jwt.sign(
        {
            user_id: user.id,
            auth_time: getTimestamp(new Date().toLocaleString()),
            subject: user.id,
            name: user.displayName,
            photoURL: user.photoURL,
            isAdmin: user.isAdmin,
            email: user.email,
            email_verified: true,
        },
        privateKey,
        signOptions
    );
}


@Injectable()
export class UserService {
    constructor(
        @InjectModel('User') private readonly UserModel: Model<User>
    ) { }


    async findOneTest(email: string): Promise<User> | undefined {
        const user = await this.UserModel.findOne({ email: email.trim() });
        return user;
    }

    async insertUser(req, res) {


        try {

            const salt = await bcrypt.genSalt(10);
            // console.log(salt);
            const CodePassword = await bcrypt.hash(req.body.password, salt);

            if (typeof req.body.displayName !== "string") {
                res.status(400).json("User is defined")
            }

            // const email = await this.UserModel.findByEmail({ username: req.body.username.trim() })

            // console.log(this.UserModel);

            const displayName = await this.UserModel.findOne({ displayName: req.body.displayName.trim() })
            displayName && res.status(400).json("displayName has been used")

            const user = await this.UserModel.findOne({ email: req.body.email.trim() });
            user && res.status(400).json("Email has been used");


            const newUser = new this.UserModel({
                displayName: req.body.displayName,
                email: req.body.email,
                password: CodePassword,
            });



            const result = await newUser.save();
            res.status(200).json(result)
            res.status(200).json("Successful")
            return result.id as string;

        } catch (err) {
            res.status(500).json(err)
        }

    }

    async getAllUser() {
        const UserRepository = this.UserModel;

        const user = await UserRepository.find();

        return user.map(prod => ({
            id: prod.id,
            displayName: prod.displayName,
            email: prod.email,
            password: prod.password,
            phoneNumber: prod.phoneNumber,
            desc: prod.desc,
            from: prod.from,
            city: prod.city,
            isAdmin: prod.isAdmin,
            photoURL: prod.photoURL,
            coverPhotoURL: prod.coverPhotoURL,
        }))
    }

    async getSingleUser(UserId: string) {
        const user = await this.findUser(UserId);
        return {
            id: user.id,
            username: user.displayName,
            phoneNumber: user.phoneNumber,
            coverPhotoURL: user.coverPhotoURL,
            photoURL: user.photoURL,
            desc: user.desc,
            isAdmin: user.isAdmin,
            city: user.city,
            from: user.from,
        }
    }

    async updateUser(
        UserId: string,
        displayName: string,
        phoneNumber: string,
        desc: string,
        from: string,
        city: string,
        isAdmin: boolean,
        photoURL: string,
        coverPhotoURL: string,
    ) {
        const user = await this.findUser(UserId);
        const updatedUser = await this.findUser(UserId);

        if (displayName) {
            updatedUser.displayName = displayName;
        }
        if (phoneNumber) {
            updatedUser.phoneNumber = phoneNumber;
        }
        if (desc) {
            updatedUser.desc = desc;
        }
        if (from) {
            updatedUser.from = from;
        }
        if (city) {
            updatedUser.city = city;
        }
        if (photoURL) {
            updatedUser.photoURL = photoURL;
        }
        if (coverPhotoURL) {
            updatedUser.coverPhotoURL = coverPhotoURL;
        }
        if (isAdmin) {
        }
        updatedUser.save();
    }

    // ---------------------- ChangePassword ----------------------


    async ChangePassword(password: string, passwordOld: string, res, req) {


        const verify = (req, res) => {
            const authHeader = req.headers['authorization'];
            // <!-- Bearer acsessToken -->

            if (authHeader) {
                //   <!-- ...some thing hear -->
                const token = authHeader.split(" ")[1];
                jwt.verify(token, publicKey, verifyOptions, async (err, users) => {
                    if (err) {
                        return res.status(401).json("Token is not Valid");
                    }
                    // req.user = user
                    // console.log(users)
                    const UserId = users["user_id"]
                    const salt = await bcrypt.genSalt(10);
                    const user = await this.findUser(UserId);
                    const CodePassword = await bcrypt.hash(password, salt);
                    const validPassword = await bcrypt.compare(passwordOld, user.password)

                    // console.log(password)

                    if (validPassword) {
                        user.password = CodePassword;
                        res.status(200).json(user)
                    } else {
                        res.status(400).json("wrong password")
                    }
                    user.save();

                });
            } else {
                res.status(400).json("You are not authentication")
            }
        }

        verify(req, res)

        // if (token) {
        //     jwt.verify(token, publicKey, verifyOptions, async (err, users) => {
        //         if (err) {
        //             return res.status(401).json("Token is not Valid");
        //         }
        //         console.log(users)
        //         const salt = await bcrypt.genSalt(10);
        //         const user = await this.findUser(UserId);
        //         const CodePassword = await bcrypt.hash(password, salt);
        //         const validPassword = await bcrypt.compare(passwordOld, user.password)

        //         // console.log(password)

        //         if (validPassword) {
        //             user.password = CodePassword;
        //             res.status(200).json(user)
        //         } else {
        //             res.status(400).json("wrong password")
        //         }
        //         user.save();
        //         // next();

        //     });

        // } else {
        //     res.status(400).json("You are not authentication")
        // }

    }



    // -------------------- Login -----------------------------


    async Login(req, res) {
        // console.log(res)
        try {
            // console.log(req.body.email.trim());
            const user: any = await this.UserModel.findOne({ email: req.body.email.trim() });
            !user && res.status(401).json("Wrong password or username!");

            const validPassword = await bcrypt.compare(req.body.password.trim(), user.password)
            !validPassword && res.status(401).json("Wrong password or username!")



            // console.log(accessToken);
            // <!-- Generate an access token  -->
            if (user && validPassword) {
                const accessToken = generateAccessToken(user);
                // console.log(process.env.SecretKey);
                // const refreshToken = generateRefreshToken(user)

                // var verified = jwt.verify(accessToken, publicKey, verifyOptions);
                // console.log("\n Verified: " + JSON.stringify(verified));

                // refreshToken.push(refreshToken);

                // <!-- expiresIn thời gian tồn tại của cái acsess token 4 day -->

                const { password, ...info } = user._doc;
                // console.log(...info);

                res.status(200).json({ user: info, accessToken: accessToken })
            }


        } catch (err) {
            res.status(500).json(err)
        }

    }

    // -------------------- FindUser ---------------------------
    private async findUser(id: string): Promise<User> {
        let user;
        try {
            user = await this.UserModel.findById(id).exec();
        } catch (error) {
            throw new NotFoundException('Could not find user.');
        } if (!user) {
            throw new NotFoundException('Could not find user')
        }
        return user;
    }

}
