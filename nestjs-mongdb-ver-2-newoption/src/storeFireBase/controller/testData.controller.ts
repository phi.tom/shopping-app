import { BadRequestException, Body, Controller, Delete, Get, HttpStatus, Param, Patch, Post, Req, Res, UnauthorizedException } from '@nestjs/common';
import { Request, response } from 'express';
import { DataTest } from '../dto/testData.dto';
import { DataTestService } from '../service/dataTest.service'

@Controller("/data/firebase/test")

export class TestDataController {

    constructor(private readonly dataTestService: DataTestService) { }

    @Get()
    async fillAll() {
        // console.log("here")
        const data = this.dataTestService.fillAll();
        return data
    }

    @Get("/:id")
    async getDataById(
        @Param('id') prodId: string,
        @Res() res
    ) {
        const data = this.dataTestService.getDataById(prodId, res);

        return data
    }

    @Post()
    async sendData(
        @Body('name') prodName: string,
        @Body('desc') prodDesc: string,
        @Res() res
    ) {
        const response = await this.dataTestService.sendDataFireBase(prodName, prodDesc, res);

        // console.log(response)
        return response
    }

    @Patch('/:id')
    async updateData(
        @Param('id') prodId: string,
        @Req() req,
        @Res() res
    ) {
        const response = await this.dataTestService.updateDataTest(prodId, req, res);
        return response
    }

    @Delete('/:id')
    async deleteData(
        @Param('id') prodId: string,
        @Res() res
    ) {
        const response = await this.dataTestService.deleteData(prodId, res);
        return response
    }

}

