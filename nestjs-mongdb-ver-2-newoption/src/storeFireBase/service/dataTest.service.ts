import { Injectable, UnauthorizedException } from '@nestjs/common';
import { DataTest } from '../dto/testData.dto';
// import { getStorage } from 'firebase-admin/storage';

import * as admin from 'firebase-admin'
// import firebase from 'firebase'

@Injectable()

export class DataTestService {

    private readonly dataTest: DataTest[] = []

    async sendDataFireBase(name: string, desc: string, res): Promise<any> {
        // console.log(name, desc)

        const db = admin.database();

        const dbStorages = admin.firestore()

        // set(data) ==> send data to firebase

        try {
            await dbStorages.collection('datatest').doc().set({
                name: name,
                desc: desc,
            })
            res.status(200).jsonp({
                name: name,
                desc: desc,
            })
        } catch (err) {
            res.status(400).jsonp({ msg: err })
        }

        // firebase realtime

        // await db.ref("datatest").set({
        //     name: name,
        //     desc: desc,
        // });

    }

    async fillAll(): Promise<DataTest[]> {

        // const images = getStorage()

        const db = admin.firestore();

        const storage = admin.storage();

        // console.log(storage.get(''));

        // console.log((await db.collection('datatest').get()).docs.map(data => { return data.data() }));

        (await admin.firestore().collection('/datatest').get()).docs.map(data => {
            // console.log(data.data());

            this.dataTest.push({
                name: data.get('name'),
                desc: data.get('desc'),
            });
        });

        return this.dataTest;
    }

    async getDataById(id: any, res): Promise<any> {

        // console.log(id)
        const dbStorages = admin.firestore();
        try {

            const data = await dbStorages.collection('datatest').doc(id);
            const value = await data.get();

            if (!value.exists) {
                res.status(404).jsonp('Data with the given Id not found')
            } else {
                res.status(200).jsonp({ data: value.data() })
            }
        } catch (err) {
            res.status(400).jsonp({ msg: err })
        }

    }

    async updateDataTest(id: any, req, res): Promise<any> {

        const dbStorages = admin.firestore();
        const valueUpdate = req.body;
        console.log(valueUpdate)
        try {
            const data = await dbStorages.collection('datatest').doc(id);
            const value = await data.get();
            if (!value.exists) {
                res.status(404).jsonp('Data with the given Id not found')
            } else {
                await data.update(valueUpdate);
                res.status(200).jsonp({ data: req.body })
            }
        } catch (err) {
            res.status(400).jsonp({ msg: err })
        }
    }

    async deleteData(id: any, res): Promise<any> {
        const dbStorages = admin.firestore()

        try {
            const data = await dbStorages.collection('datatest').doc(id);
            const value = await data.get();
            if (!value.exists) {
                res.status(404).jsonp('Data with the given Id not found')
            } else {
                data.delete();
                res.status(200).jsonp('Data has been deleted')
            }

        } catch (err) {
            res.status(400).jsonp({ msg: err })
        }
    }

}