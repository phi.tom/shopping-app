import { TopProductsModule } from './topproducts/topproducts.module';
import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ProductsModule } from './products/products.module';
import { UserModule } from './user/user.module'
import { GraphQLModule } from '@nestjs/graphql';
import { ConfigModule } from '@nestjs/config'
import 'dotenv/config'
import { AuthModule } from './auth/auth.module'
import { CmtProductModule } from './cmtProduct/cmtProduct.module'
import { TestDataModule } from './storeFireBase/module/testData.module'

const URL = process.env.MONGOOSE_DATABASE_URL
// console.log(URL);

@Module({
  imports: [
    ConfigModule.forRoot(),
    GraphQLModule.forRoot({
      debug: false,
      autoSchemaFile: true,

    }),
    ProductsModule,
    TopProductsModule,
    UserModule,
    AuthModule,
    CmtProductModule,
    TestDataModule,
    MongooseModule.forRoot(URL, {
      autoCreate: true
    }),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule { }
