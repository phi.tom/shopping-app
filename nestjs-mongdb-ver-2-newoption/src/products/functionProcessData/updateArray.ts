export const UpdateDataArray = (data1: any[], data2: any[], type: any) => {

    const arr = data1.concat(data2).reduce((prev, current, index, array) => {

        // console.log(index)

        // khoi tao prev result: [], keys: {} ==> result [ { size: 'M', QuantityProduct: 10 }]
        // ==> keys == "M"
        // sau do prev [ { size: 'M', QuantityProduct: 10 },{ size: 'L', QuantityProduct: 10 }] ==> keys = 'M' , "L"

        // final
        //  result: [
        //  { size: 'M', QuantityProduct: 10 },
        //  { size: 'L', QuantityProduct: 2 },
        //  { size: 'XL', QuantityProduct: 1 },
        //  { size: '2XL', QuantityProduct: 5 }
        //  ],
        // keys: { M: 0, L: 1, XL: 2, '2XL': 3 }

        // current
        //  { size: 'M', QuantityProduct: 10 }
        //  { size: 'L', QuantityProduct: 2 }
        //  { size: 'XL', QuantityProduct: 1 }
        //  { size: '2XL', QuantityProduct: 5 }
        //  { size: 'M', QuantityProduct: 10 }

        // index = length result + length keys 

        if (!(current[type] in prev.keys)) {
            // console.log(prev.keys[current.size] = index)
            prev.keys[current[type]] = index; // gan lai index
            prev.result.push(current);
            // push du lieu vao mang result
        }
        else {
            // neu co roi toi set lai gia tri
            prev.result[prev.keys[current[type]]] = current;
        }

        // console.log(prev.result[prev.keys[current.size]] = current)

        // if current.size !== prev.keys=(M) ==> push vao array
        // console.log(prev.keys, current.size, current)

        return prev;
    }, { result: [], keys: {} }).result;

    return arr

}