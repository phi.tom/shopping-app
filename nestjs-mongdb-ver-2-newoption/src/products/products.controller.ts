import {
  Controller,
  Post,
  Body,
  Get,
  Param,
  Patch,
  Delete,
  Put,
  Req,
  Res,
} from '@nestjs/common';

import { ProductsService } from './products.service';

@Controller('/api/products')
export class ProductsController {
  constructor(private readonly productsService: ProductsService) { }

  @Post()
  async addProduct(
    @Body('name') prodName: string,
    @Body('brand') prodBrand: string,
    @Body('descProduct') prodDescProduct: string,
    @Body('OldPrice') prodOldPrice: number,
    @Body('NewPrice') prodNewPrice: number,
    @Body('ColorProduct') prodColorProduct: string[],
    @Body('ImgUrlProduct') prodImgUrlProduct: {},
    @Body('RateProduct') prodRateProduct: object[],
    @Body('QuantityProductAndSize') prodQuantityProductAndSize: any[],
    @Body('Categories') prodCategories: string[],
    @Body('commentId') prodCommentId: string,

  ) {
    const generatedId = await this.productsService.insertProduct(
      prodName,
      prodBrand,
      prodDescProduct,
      prodOldPrice,
      prodNewPrice,
      prodColorProduct,
      prodImgUrlProduct,
      prodRateProduct,
      prodQuantityProductAndSize,
      prodCategories,
      prodCommentId,
    );
    return { id: generatedId };
  }

  @Get('/all')
  async getAllProduct() {
    const products = await this.productsService.getAllProducts();
    return products;
  }

  @Get()
  async getAllProducts(@Req() req, @Res() res) {
    // console.log(req)
    const products = await this.productsService.getProducts(req.query, res);
    return res;
  }

  @Get('/product/:id')
  getProduct(@Param('id') prodId: string) {
    return this.productsService.getSingleProduct(prodId);
  }

  @Patch('/product/:id')
  async updateProduct(
    @Param('id') prodId: string,
    @Body('name') prodName: string,
    @Body('brand') prodBrand: string,
    @Body('descProduct') prodDescProduct: string,
    @Body('OldPrice') prodOldPrice: number,
    @Body('NewPrice') prodNewPrice: number,
    @Body('ColorProduct') prodColorProduct: string[],
    @Body('ImgUrlProduct') prodImgUrlProduct: {},
    @Body('RateProduct') prodRateProduct: object[],
    @Body('QuantityProductAndSize') prodQuantityProductAndSize: any[],
    @Body('Categories') prodCategories: string[],
    @Body('commentId') prodCommentId: string,
  ) {
    await this.productsService.updateProduct(prodId,
      prodName,
      prodBrand,
      prodDescProduct,
      prodOldPrice,
      prodNewPrice,
      prodColorProduct,
      prodImgUrlProduct,
      prodRateProduct,
      prodQuantityProductAndSize,
      prodCategories,
      prodCommentId,
    );
    return "Complete Updated Product";
  }

  @Delete('/product/:id')
  async removeProduct(@Param('id') prodId: string, @Res() res) {
    await this.productsService.deleteProduct(prodId, res);
    return res
  }
}
