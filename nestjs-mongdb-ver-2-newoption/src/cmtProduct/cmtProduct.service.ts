import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CmtProduct } from './cmtProduct.model'
import { Comment } from './cmtProduct.interface'

@Injectable()
export class CmtProductService {
    constructor(
        @InjectModel('CmtProduct') private readonly cmtProductModel: Model<CmtProduct>,
    ) { }

    // PostMan

    async insertCmtProduct(productId: string, commentProduct: object[]) {
        const newCmtProduct = new this.cmtProductModel({
            productId: productId,
            commentProduct: commentProduct
        });
        const result = await newCmtProduct.save();
        return result.id as string;
    };



    async getAllComment() {

        const commentRepository = this.cmtProductModel;
        const comment = await commentRepository.find();
        return comment.map(prod => ({
            _id: prod._id,
            productId: prod.productId,
            commentProduct: prod.commentProduct,
        }));
    }

    async getSingleCommentProduct(commentId: string) {
        const comment = await this.CommentProduct(commentId);
        return {
            _id: comment._id,
            productId: comment.productId,
            commentProduct: comment.commentProduct,
        };
    }

    async getCheckExistIdProduct(productId: string) {
        const comment = await this.cmtProductModel.find({ productId: productId });
        // console.log(comment)
        if (comment.length == 1) {
            return true
        } else {
            return false
        }
    }

    async updateComment(commentId: string, updateData: object[]) {
        const updateComment = await this.CommentProduct(commentId);

        if (updateData) {
            const ArrayData = updateComment.commentProduct;
            const newData = [...ArrayData];
            newData.push(...updateData)
            // console.log(newData)
            updateComment.commentProduct = newData;

        } else {
            throw new NotFoundException('You forget data update!')
        }

        updateComment.save()

        return updateComment;
    }

    async updateCommentDetail(commentId: string, updateData: Comment[], prodIdValidate2: string) {
        const updateComment = await this.CommentProduct(commentId);

        // console.log(updateComment)

        if (updateComment && updateData) {
            // console.log(updateData[0].userCmt)
            const userId = updateData[0].userId
            const comment = await updateComment.commentProduct.filter(user => user.userId === userId && user._id == prodIdValidate2);
            // console.log(comment[0].userCmt)
            comment[0].userCmt = updateData[0].userCmt
            updateComment.save()
            return comment
        } else {
            throw new NotFoundException('You forget data update!')
        }


    }

    async getCommentByProductId(productId: string, prodIdValidate: string) {
        const comment = await this.cmtProductModel.find({ productId: productId });
        if (comment[0]._id == prodIdValidate) return comment;
        else throw new NotFoundException('Could not find comment.');
        // const commentCheck = await this.CommentProduct();
    }

    async getCommentProductId(productId: string) {
        const comment = await this.cmtProductModel.find({ productId: productId });
        return comment
    }

    async deleteAllComment(prodId: string, res) {
        const result = await this.cmtProductModel.deleteOne({ _id: prodId }).exec();
        // console.log(result.deletedCount)
        if (result.deletedCount === 1) {
            res.status(200).jsonp("Data Product has been deleted");
        }
        if (result.deletedCount === 0) {
            throw new NotFoundException('Could not find product.');
        }
    }

    async deleteOneComment(commentId: string, updateData: Comment[], prodIdValidate2: string, res) {

        const updateComment = await this.CommentProduct(commentId);

        // console.log(updateComment)

        if (updateComment && updateData) {

            const userId = updateData[0].userId
            const response = await updateComment.commentProduct.filter(user => user.userId === userId && user._id == prodIdValidate2);
            // console.log([response[0]])

            if (response[0] === undefined) {
                res.status(400).jsonp(" Data Comment not found!");
            } else {
                const datafind = await this.cmtProductModel.updateMany({ $pull: { commentProduct: { _id: prodIdValidate2 } } });
                res.status(200).jsonp("Data Comment has been deleted!");
            }

        } else {
            throw new NotFoundException('You forget data deleted!')
        }

    }

    private async CommentProduct(id: string) {
        let commentProduct;
        try {
            commentProduct = await this.cmtProductModel.findById(id).exec();
        } catch (error) {
            throw new NotFoundException('Could not find comment.');
        }
        if (!commentProduct) {
            throw new NotFoundException('Could not find comment.');
        }
        return commentProduct;
    }

}