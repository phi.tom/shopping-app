import {
    Controller,
    Post,
    Body,
    Get,
    Param,
    Patch,
    Delete,
    Put,
    Req,
    Res,
    NotFoundException,
} from '@nestjs/common';

import { CmtProductService } from "./cmtProduct.service"
import { ProductsService } from '../products/products.service'
import { Comment } from './cmtProduct.interface'


@Controller('/api/cmtProduct')
export class CmtProductController {
    constructor(private readonly CmtProductService: CmtProductService,
        private readonly productsService: ProductsService
    ) { }

    @Get()
    async getAllComments() {
        const Response = await this.CmtProductService.getAllComment();
        return Response;
    };

    @Get("/:id")
    async getSingleComment(
        @Param('id') prodId: string
    ) {
        return await this.CmtProductService.getSingleCommentProduct(prodId);
    };

    @Get("byProduct/:id")
    async getCommentByProductId(
        @Param('id') prodId: string,
        @Body('_id') prodIdValidate: string
    ) {
        return await this.CmtProductService.getCommentByProductId(prodId, prodIdValidate);
    };

    @Patch("byProduct/:id")
    async ChangeComment(
        @Param('id') prodId: string,
        @Body('_id') prodIdValidate: string,
        @Body("commentProduct") commentProduct: Comment[],
        @Body("idValidate") prodIdValidate2: string
    ) {
        const data = await this.CmtProductService.getCommentByProductId(prodId, prodIdValidate);
        if (data) {
            const Response = await this.CmtProductService.updateCommentDetail(prodIdValidate, commentProduct, prodIdValidate2)
            // console.log(dataUpdate)
            return Response
        }
    };

    @Get()
    async getCommentProductId(prodProductId: string) {
        const Response = await this.CmtProductService.getCommentProductId(prodProductId);
        return Response;
    };

    @Get("product/:id")
    async getAllCommentProductId(
        @Param('id') prodId: string,
    ) {
        const Response = await this.CmtProductService.getCommentProductId(prodId);
        return Response;
    };

    @Patch()
    async UpdatecommentId(prodProductId: string, generatedId: string) {
        const Response = await this.productsService.updateProductCommentId(prodProductId, generatedId)
        return Response
    };

    @Patch("comment/:id")
    async UpdateComment(
        @Param('id') prodId: string,
        @Body('commentProduct') prodCommentProduct: object[],
    ) {
        const Response = await this.CmtProductService.updateComment(prodId, prodCommentProduct)
        return Response
    };

    @Post()
    async addCmtProduct(
        @Body('productId') prodProductId: string,
        @Body('commentProduct') prodCommentProduct: object[],
    ) {

        const validate = await this.CmtProductService.getCheckExistIdProduct(prodProductId)

        if (validate) {
            const data = await this.getCommentProductId(prodProductId);
            const id = data[0]._id
            const Response = await this.UpdateComment(id, prodCommentProduct)
            return { msg: "Id Product has been exists", data: data, dataNew: Response };
            // return "Id Product has been exists"
        } else {
            const generatedId = await this.CmtProductService.insertCmtProduct(
                prodProductId,
                prodCommentProduct
            );

            const Response = await this.UpdatecommentId(prodProductId, generatedId)
            return { id: generatedId, msg: Response };
        }
    };

    @Delete("allComment/:id")
    async deleteAllComment(
        @Param('id') prodId: string,
        @Res() res
    ) {
        await this.CmtProductService.deleteAllComment(prodId, res);
        return res
    };

    @Patch("oneComment/:id")
    async deleteOneComment(
        @Param('id') prodId: string,
        @Body('_id') prodIdValidate: string,
        @Body("commentProduct") commentProduct: Comment[],
        @Body("idValidate") prodIdValidate2: string,
        @Res() res,
    ) {
        const data = await this.CmtProductService.getCommentByProductId(prodId, prodIdValidate);
        if (data) {
            const Response = await this.CmtProductService.deleteOneComment(prodIdValidate, commentProduct, prodIdValidate2, res);
            // console.log(dataUpdate)
            return Response
        }

    }

}