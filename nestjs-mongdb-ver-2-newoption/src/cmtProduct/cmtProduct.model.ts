import * as mongoose from 'mongoose'

export const userCmt = new mongoose.Schema({
    userId: { type: String, required: true },
    displayName: { type: String, required: true },
    photoURL: { type: String, default: "" },
    userCmt: { type: String, default: "" },
},
    { timestamps: true }
)

export const CmtProductSchema = new mongoose.Schema({
    productId: { type: String, required: true },
    commentProduct: { type: [userCmt], required: true },
})

export interface CmtProduct extends mongoose.Document {
    id: string;
    productId: string;
    commentProduct: Object[];
}
