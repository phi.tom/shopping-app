<p align="center">
  <a href="http://nestjs.com/" target="blank"><img src="https://nestjs.com/img/logo_text.svg" width="320" alt="Nest Logo" /></a>
</p>

[travis-image]: https://api.travis-ci.org/nestjs/nest.svg?branch=master
[travis-url]: https://travis-ci.org/nestjs/nest
[linux-image]: https://img.shields.io/travis/nestjs/nest/master.svg?label=linux
[linux-url]: https://travis-ci.org/nestjs/nest
  
  <p align="center">A progressive <a href="http://nodejs.org" target="blank">Node.js</a> framework for building efficient and scalable server-side applications, heavily inspired by <a href="https://angular.io" target="blank">Angular</a>.</p>
    <p align="center">
<a href="https://www.npmjs.com/~nestjscore"><img src="https://img.shields.io/npm/v/@nestjs/core.svg" alt="NPM Version" /></a>
<a href="https://www.npmjs.com/~nestjscore"><img src="https://img.shields.io/npm/l/@nestjs/core.svg" alt="Package License" /></a>
<a href="https://www.npmjs.com/~nestjscore"><img src="https://img.shields.io/npm/dm/@nestjs/core.svg" alt="NPM Downloads" /></a>
<a href="https://travis-ci.org/nestjs/nest"><img src="https://api.travis-ci.org/nestjs/nest.svg?branch=master" alt="Travis" /></a>
<a href="https://travis-ci.org/nestjs/nest"><img src="https://img.shields.io/travis/nestjs/nest/master.svg?label=linux" alt="Linux" /></a>
<a href="https://coveralls.io/github/nestjs/nest?branch=master"><img src="https://coveralls.io/repos/github/nestjs/nest/badge.svg?branch=master#5" alt="Coverage" /></a>
<a href="https://gitter.im/nestjs/nestjs?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge&utm_content=body_badge"><img src="https://badges.gitter.im/nestjs/nestjs.svg" alt="Gitter" /></a>
<a href="https://opencollective.com/nest#backer"><img src="https://opencollective.com/nest/backers/badge.svg" alt="Backers on Open Collective" /></a>
<a href="https://opencollective.com/nest#sponsor"><img src="https://opencollective.com/nest/sponsors/badge.svg" alt="Sponsors on Open Collective" /></a>
  <a href="https://paypal.me/kamilmysliwiec"><img src="https://img.shields.io/badge/Donate-PayPal-dc3d53.svg"/></a>
  <a href="https://twitter.com/nestframework"><img src="https://img.shields.io/twitter/follow/nestframework.svg?style=social&label=Follow"></a>
</p>
  <!--[![Backers on Open Collective](https://opencollective.com/nest/backers/badge.svg)](https://opencollective.com/nest#backer)
  [![Sponsors on Open Collective](https://opencollective.com/nest/sponsors/badge.svg)](https://opencollective.com/nest#sponsor)-->

## Description

[Nest](https://github.com/nestjs/nest) framework TypeScript starter repository.

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

## Support

Nest is an MIT-licensed open source project. It can grow thanks to the sponsors and support by the amazing backers. If you'd like to join them, please [read more here](https://docs.nestjs.com/support).

## Stay in touch

## License

  Nest is [MIT licensed](LICENSE).

## unique Các dữ liệu trong trường không được trùng nhau

--- create Schema   


# Using Graphql --> yarn add @nestjs/graphql graphql apollo-server-express

Go to app.module.ts ==> add import { GraphQLModule } from '@nestjs/graphql';

Create Resolve file ==> Explicit types Data send to server and read from database (input and dto)

Query <=> Get ...

Mutation <=> Put , Patch , Post 

Fields() : Lĩnh vực , Trường


# Start Working Graphql

Apollo client là một thư viện nằm ở phía fe có nhiệm vụ nói chuyện với graphql ở phía be

-- Query : (Yêu cầu truy xuất dữ liệu(đọc dữ liệu))

Example:

      // Schema

      type Book {
        id: ID
        name: String
        genre: String
        author: Author, // cầu nối giữ quyển sách và tác giả
      }

      type Author{
        id: ID!
        name: String
        age: Int
        books: [Book]
      }

      # Root Type (Yêu câu truy xuất dữ liệu)
      type Query{
        books: [Book] // danh sách của những quyển sách
        book(id: ID!): Book
        authors: [Author]
        author(id: ID!): Author
      }

      <!-- Đưa một cái gì đó vào cơ sở dữ liệu -->
      type Mutation{
          createAuthor(
            id: ID!,
            name: String,
            age: Int
            ): Author
          createBook{
            id: ID!
            name: String,
            genre : String,
            authorId" ID!
          }: Book

      }

      //  Resolve trả về dữ liệu chi tiết
      //  Có thể đọc từ data base 
          Query:{ 
            books: () => [
              {
                id: 1, 
                name: "....",
                genre: "Adventure",
                authorId: 1,
              },
               {
                id: 2, 
                name: "....",
                genre: "Education",
                 authorId: 2,
              },
               {
                id: 3, 
                name: "....",
                genre: "Romantic",
                authorId: 3, 
              },
            ],
            book: (parent,args) => books.find(book => book.id == args.id),
            author: () => [
              {
                id: 1,
                name: "...",
                age: 50,
              },
              {
                id: 2,
                name: "...",
                age: 50,
              },
              {
                id: 3,
                name: "...",
                age: 50,
              },

            ],
            author: (parent,args) => authors.find(author => author.id == args.id)
          },
          <!-- Xử lý trường author trong book -->
          Book: {
              author: (parent,args) => {
                console.log(parent) // == kết quả tìm được từ query cha

                return authors.find(author => author.id == parent.authorId) 
              } 
          }

          Author: {
            books: (parent,args) => {
              return books.filter(book => book.authorId == parent.id)
            }
          }

          <!-- Mutation -->

          Mutation: {
            createAuthor: (parent,args) => args, // chỉ trả về dữ liệu truyền vào
            createBook: (parent,args) => args,
          }


      

       <!-- Nối dữ liệu bằng graphql -->

# <!-- Learn Mongoose -->

Mongoose models provide several static helper functions for CRUD operations. Each of these functions returns a mongoose Query object.

  Model.deleteMany()
  Model.deleteOne()
  Model.find()
  Model.findById()
  Model.findByIdAndDelete()
  Model.findByIdAndRemove()
  Model.findByIdAndUpdate()
  Model.findOne()
  Model.findOneAndDelete()
  Model.findOneAndRemove()
  Model.findOneAndReplace()
  Model.findOneAndUpdate()
  Model.replaceOne()
  Model.updateMany()
  Model.updateOne()


# Learn More https://mongoosejs.com/docs/index.html